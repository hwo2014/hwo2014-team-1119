var GameState = function() {
  this.cars = {};
  this.track = {};
  this.raceSession = {};
  this.ownCar = {};
}

GameState.prototype.setOwnCar = function(name, color) {
  this.ownCar = {"color" : color, "name" : name };
}

GameState.prototype.getOwnCarData = function() {
  var color = this.ownCar.color;
  return this.cars[color];
}

GameState.prototype.isCurvePiece = function(pieceIndex) {
  return !!this.track.pieces[pieceIndex].radius;
}

GameState.prototype.isInitialized = function() {
  return !!this.track.pieces;
}
GameState.prototype.getCurvePieceRadius = function(pieceIndex, lane) {
  var piece = this.track.pieces[pieceIndex];
  return ((piece.radius + this.track.lanes[lane.startLaneIndex].distanceFromCenter) + (piece.radius + this.track.lanes[lane.endLaneIndex].distanceFromCenter)) / 2;
}

GameState.prototype.nextPiecesIndices = function(firstIndex, count) {
  var ret = [];
  for (var i = 0; i < count; i++) {
    ret.push(firstIndex);
    firstIndex++;
    if (firstIndex == this.track.pieces.length) firstIndex = 0;
  }
  return ret;
}

/*
"lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      }
*/

//TODO: Fix lane switch?
GameState.prototype.pieceLength = function(pieceIndex, lane) {
  var piece = this.track.pieces[pieceIndex];

  var w = this.track.lanes[lane.startLaneIndex].distanceFromCenter - this.track.lanes[lane.endLaneIndex].distanceFromCenter;
  if (piece.length) {
    return Math.sqrt(w * w + piece.length * piece.length);
  }
  var add = (this.track.lanes[lane.startLaneIndex].distanceFromCenter + this.track.lanes[lane.endLaneIndex].distanceFromCenter) / 2;
  if (piece.angle > 0) add = -add;
  var length = 2 * Math.PI * (piece.radius + add) * (piece.angle / 360.0);
  return Math.sqrt(w * w + length * length);
}

GameState.prototype.distanceToPiece = function(startPiece, inPieceDistance, endPiece, laneIndex) {
  var lane = {
    "startLaneIndex": laneIndex,
    "endLaneIndex": laneIndex
  }
  if (startPiece < endPiece) {
    var dist = -inPieceDistance;
    for (var i = startPiece; i < endPiece;i++) {
      dist += this.pieceLength(i, lane);
    }
    return dist;
  }
  var dist = -inPieceDistance;
  for (var i = startPiece; i < this.track.pieces.length; i++) {
    dist += this.pieceLength(i, lane);
  }
  for (var i = 0; i < endPiece; i++) {
    dist += this.pieceLength(i, lane);
  }
  return dist;
}

GameState.prototype.distanceToPieceEnd = function(piecePosition) {
  var index = piecePosition.pieceIndex;
  var pieceLength = this.pieceLength(index, piecePosition.lane);
  return pieceLength - piecePosition.inPieceDistance;
}

GameState.prototype.carMovementSpeed = function(piecePosition1, piecePosition2) {
  if (piecePosition1.pieceIndex === piecePosition2.pieceIndex) {
    return piecePosition2.inPieceDistance - piecePosition1.inPieceDistance;
  }
  var dist = this.distanceToPieceEnd(piecePosition1);
  return dist + piecePosition2.inPieceDistance;
}

GameState.prototype.updateCarPositions = function(data) {
  var self = this;
  data.forEach(function(carPos) {
    var car = self.cars[carPos.id.color];
    if (car.piecePosition) {
      var vel = self.carMovementSpeed(car.piecePosition, carPos.piecePosition);
      if (vel > 0) {
       car.vel = vel;
      }
    } else {
      car.vel = 0;
    }
    car.angle = carPos.angle;
    car.piecePosition = carPos.piecePosition;
  });
}

GameState.prototype.carCrashed = function(carColor) {
  this.cars[carColor].crashed = true;
}

GameState.prototype.carSpawned = function(carColor) {
  this.cars[carColor].crashed = false;
}

/*
  "track": {
    "id": "indianapolis",
    "name": "Indianapolis",
    "pieces": [
      {
        "length": 100.0
      },
      {
        "length": 100.0,
        "switch": true
      },
      {
        "radius": 200,
        "angle": 22.5
      }
    ],
    "lanes": [
      {
        "distanceFromCenter": -20,
        "index": 0
      },
      {
        "distanceFromCenter": 0,
        "index": 1
      },
      {
        "distanceFromCenter": 20,
        "index": 2
      }
    ],
    "startingPoint": {
      "position": {
        "x": -340.0,
        "y": -96.0
      },
      "angle": 90.0
    }
  }
  "cars": [
    {
      "id": {
        "name": "Schumacher",
        "color": "red"
      },
      "dimensions": {
        "length": 40.0,
        "width": 20.0,
        "guideFlagPosition": 10.0
      }
    },
    {
      "id": {
        "name": "Rosberg",
        "color": "blue"
      },
      "dimensions": {
        "length": 40.0,
        "width": 20.0,
        "guideFlagPosition": 10.0
      }
    }
  ],
  "raceSession": {
    "laps": 3,
    "maxLapTimeMs": 30000,
    "quickRace": true
  }
*/

GameState.prototype.gameInit = function(track, cars, raceSession) {
  var self = this;
  cars.forEach(function(car) {
    self.cars[car.id.color] = car;
  });

  this.track = track;
  this.raceSession = raceSession;
}



module.exports = GameState;