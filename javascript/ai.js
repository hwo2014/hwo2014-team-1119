var AI = function(gameState) {
  this.gameState = gameState;
  this.curveSpeedCoeff = 0.5;
  this.brakingPower = 0.4;
  this.lastThrottle = 1.0;
  this.throttleChangeHardness = 1.0;
}

AI.prototype.calculatePieceSpeedCoeff = function(pieceIndex, lane) {
  if (!this.gameState.isCurvePiece(pieceIndex)) {
    return Number.POSITIVE_INFINITY;
  }

  var radius = this.gameState.getCurvePieceRadius(pieceIndex, lane);
  return Math.sqrt(radius * this.curveSpeedCoeff); 
}

AI.prototype.nextPiecesSpeedCoeffs = function(firstPiece, laneIndex) {
  var indices = this.gameState.nextPiecesIndices(firstPiece, 3);
  var speedCoeffs = [];
  var self = this;
  var lane = {"startLaneIndex" : laneIndex, "endLaneIndex" : laneIndex};
  indices.forEach(function (index) {
    speedCoeffs.push({"piece" : index, "coeff" : self.calculatePieceSpeedCoeff(index, lane)});
  });
  return speedCoeffs;
}

AI.prototype.changeThrottle = function (newValue) {
  var newThrottle = this.lastThrottle + (newValue - this.lastThrottle) * this.throttleChangeHardness;
  this.lastThrottle = newThrottle;
  return newThrottle;
}
AI.prototype.distanceToPiece = function (pieceIndex) {
  var indices = gameState.nextPiecesIndices(firstPiece, 5);
}
AI.prototype.calculateThrottle = function(car, nextPiecesSpeedCoeffs) {
  var cur = nextPiecesSpeedCoeffs[0].coeff;
  var distanceToPiece = -car.piecePosition.inPieceDistance;
  var self = this;
  if (cur < car.vel /*|| car.angle > 10 || car.angle < -10*/) {
    console.log("1. braking");
    return 0;
  }
  if (!self.braking) {
    nextPiecesSpeedCoeffs.forEach(function (coeffData) {
      if (coeffData.coeff < cur) {
        self.braking = {
          "targetSpeed" : coeffData.coeff,
          "distanceToTarget" : distanceToPiece,
          "targetPiece" : coeffData.piece
        };
        return;
      }
      distanceToPiece += self.gameState.pieceLength(coeffData.piece, car.piecePosition.lane);
    });
  } else {
    var distanceToTarget = this.gameState.distanceToPiece(
      car.piecePosition.pieceIndex,
      car.piecePosition.inPieceDistance,
      this.braking.targetPiece,
      car.piecePosition.lane.startLaneIndex
    );
    console.log("A: " + (car.vel * car.vel - this.braking.targetSpeed * this.braking.targetSpeed) + " B:" + (this.brakingPower * distanceToTarget));
    if (car.vel <= this.braking.targetSpeed) {
      /*if (distanceToTarget > 0) {
        var optimalBrakingPower = this.brakingPower / ((this.braking.distanceToTarget - distanceToTarget) / this.braking.distanceToTarget);
        this.brakingPower = (this.brakingPower + optimalBrakingPower) / 2;
      }*/
      console.log("Optimal velocity vel: " + car.vel + " distanceToTarget: " + distanceToTarget);
      this.braking = false;
      return 1.0;
    } else if (this.braking.started) {
      console.log("breaking");
      return 0;
    }
    else if (car.vel * car.vel - this.braking.targetSpeed * this.braking.targetSpeed >= this.brakingPower * distanceToTarget){
      this.braking.started = true;
      console.log("breaking started vel: " + car.vel + " distanceToTarget: " + distanceToTarget);
      return 0;
    }
  }
  console.log("accelerating");
  return 1.0;
}

AI.prototype.crash = function() {
  console.log("Crashed");
  var car = this.gameState.getOwnCarData();
  var radius = this.gameState.getCurvePieceRadius(car.piecePosition.pieceIndex, car.piecePosition.lane);
  var newCoeff = (car.vel * car.vel) / radius;
  if (this.curveSpeedCoeff > newCoeff) {
    this.curveSpeedCoeff = newCoeff;
  } else {
    this.curveSpeedCoeff *= 0.9;
  }
  this.braking = false;
}

AI.prototype.spawn = function() {
  console.log("Spawned");
}

AI.prototype.update = function() {
  if (!this.gameState.isInitialized()) {
    return { "throttle" : 1.0, "switchLane" : null };
  }
  var car = this.gameState.getOwnCarData();
  if (car.crashed) {
    return { "throttle" : 1.0, "switchLane" : null };
  }

  if ((car.angle > 10.0 || car.angle < -10.0) && !this.loweredRecently) {
    this.curveSpeedCoeff *= 0.98;
    this.loweredRecently = 5;
    console.log("Lowered curveSpeedCoeff: " + this.curveSpeedCoeff);
  } else {
    if (this.loweredRecently > 0) this.loweredRecently--;
  }
  console.log(car);
  var speedCoeffs = this.nextPiecesSpeedCoeffs(car.piecePosition.pieceIndex, car.piecePosition.lane.startLaneIndex);


  var throttle = this.calculateThrottle(car, speedCoeffs);

  return { "throttle" : this.changeThrottle(throttle), "switchLane" : null };
}

module.exports = AI;