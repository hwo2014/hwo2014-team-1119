var net = require("net");
var JSONStream = require('JSONStream');

var GameState = require('./gamestate');
var AI = require('./ai');
var gameState = new GameState();
var ai = new AI(gameState);
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    gameState.updateCarPositions(data.data);
    var result = ai.update();
    if (result.switchLane) {
      send({
        "msgType": "switchLane",
        "data": result.switchLane
      });
    } else {
      send({
        msgType: "throttle",
        data: result.throttle
      });
    }
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'gameInit') {
      gameState.gameInit(data.data.race.track, data.data.race.cars, data.data.race.raceSession);
    } else if (data.msgType === 'yourCar') {
      gameState.setOwnCar(data.data.name, data.data.color);
    } else if (data.msgType === 'crash') {
      gameState.carCrashed(data.data.color);
      if (gameState.ownCar.color === data.data.color) {
        ai.crash();
      }
    } else if (data.msgType === 'spawn') {
      gameState.carSpawned(data.data.color);
      if (gameState.ownCar.color === data.data.color) {
        ai.spawn();
      }
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
